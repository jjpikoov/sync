CC=gcc
CFLAGS= -Wall -Werror -W -pthread -g3

build: red_black_tree.o main.c red_black_tree.h
	$(CC) $(CFLAGS) -o main main.c red_black_tree.o
red_black_tree.o: red_black_tree.h red_black_tree.c
	$(CC) -c $(CFLAGS) red_black_tree.c -o red_black_tree.o
run:
	./main
clean:
	rm *.o
