Problem współbieżnego dostępu do listy

    Trzy rodzaje wątków dzielą dostęp do pewnej listy. Każdy z nich wykonuje jedną z operacji: search, insert lub delete. Wątki insert dodają nowy element na końcu listy i nie mogą działać współbieżnie z innymi wątkami insert, natomiast mogą z wątkami search. Wątki delete usuwają element z dowolnego miejsca na liściei każdy taki wątek musi mieć wyłączny dostęp do listy (żadne inne wątki delete, search, insert nie mogą wtedy operować na liście). Każdy Wątek typu search może działać współbieżnie z dowolnym innym typu search.

Implementacja listy - drzewo czerwono-czarne