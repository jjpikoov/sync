#include "red_black_tree.h"


void rotate_left(red_black_tree* t, red_black_node* node) {
    red_black_node* y = node->r;
    if (y->l != NULL)
        node->r = y->l;
    
    if (y->l != NULL) {
        y->l->p = node;
    }
    y->p = node->p;
    
    if (node->p == NULL) {
        t->root = y;
    }
    else if (node == node->p->l) {
        node->p->l = y;
    }
    else {
        node->p->r = y;
    }
    
    y->l = node;
    node->p = y;
    
}


void rotate_right(red_black_tree* t, red_black_node* node) {
    red_black_node* y = node->l;
    node->l = y-> r;
    if (y->r != NULL) {
        y->r->p = node;
    }
    y->p = node->l;
    
    if (node->l == NULL) {
        t->root = y;
    }
    else if (node->p != NULL && node == node->p->r) {
        node->p->r = y;
    }
    else if (node->p != NULL) {
        node->p->l = y;
    }
    
    y->r = node;
    node->p = y;
}

void rb_tree_ins_fixup(red_black_tree* t, red_black_node* node) {
    red_black_node* y;
    
    while (node->p != NULL && node->p->c == 0) {
        if (node->p->p != NULL && node->p == node->p->p->l) {
            y = node->p->p->r;
            
            if (y != NULL && y->c == 0) {
                node->p->c = 1;
                y->c = 1;
                node->p->p->c = 0;
                node = node->p->p;
            }
            else if (node == node->p->r) {
                node = node->p;
                rotate_left(t, node);
            }
            
            if (node->p != NULL) {
                node->p->c = 1;
            }
            
            if (node->p != NULL && node->p->p != NULL) {
                node->p->p->c = 0;
                rotate_right(t, node->p->p);
            }
            
        }
        else {
            if (node->p->p != NULL)
                y = node->p->p->l;
            
            if (y != NULL && y->c == 0) {
                node->p->c = 1;
                y->c = 1;
                node->p->p->c = 0;
                node = node->p->p;
            }
            else if (node == node->p->l) {
                node = node->p;
                rotate_right(t, node);
            }
            
            if (node->p != NULL) {
                node->p->c = 1;
            }
            
            if (node->p != NULL && node->p->p != NULL) {
                node->p->p->c  = 0;
                rotate_left(t, node->p->p);
            }
            
        }
    }
    t->root->c = 1;
}


void rb_tree_del_fixup(red_black_tree* t, red_black_node* x) {
    red_black_node* w = NULL;
    
    while (x != NULL && x != t->root && x->c == 1) {
        if (x == x->p->l) {
            w = x->p->r;
            if (w->c == 0) {
                w->c = 1;
                x->p->c = 0;
                rotate_left(t, x->p);
                w = x->p->r;
            }
            if (w->l->c == 1 && w->r->c == 1) {
                w->c = 0;
                x = x->p;
            }
            else if (w->r->c == 1) {
                w->l->c = 1;
                w->c = 0;
                rotate_right(t, w);
                w = x->p->r;
            }
            
            w->c = x->p->c;
            x->p->c = 1;
            w->r->c = 1;
            rotate_left(t, x->p);
            x = t->root;
        }
        else {
            w = x->p->l;
            if (w->c == 0) {
                w->c = 1;
                x->p->c = 0;
                rotate_right(t, x->p);
                w = x->p->l;
            }
            if (w->r->c == 1 && w->l->c == 1) {
                w->c = 0;
                x = x->p;
            }
            else if (w->l->c == 1) {
                w->r->c = 1;
                w->c = 0;
                rotate_left(t, w);
                w = x->p->l;
            }
            
            w->c = x->p->c;
            x->p->c = 1;
            w->l->c = 1;
            rotate_right(t, x->p);
            x = t->root;
        }
    }
    if (x != NULL)
        x->c = 1;
}


red_black_node* rb_tree_search(red_black_tree* t, int key) {
    red_black_node* node = t->root;
    
    while (node != NULL && key != node->key) {
        if (key < node->key)
            node = node->l;
        else
            node = node->r;
    }
    return node;
}


void rb_tree_delete(red_black_tree* t, int key) {
    red_black_node* td = rb_tree_search(t, key);
    red_black_node* y = NULL;
    red_black_node* x = NULL;
    
    if (td != NULL) {
        if (td->l == NULL || td->r == NULL) {
            y = td;
        }
        else {
            y = successor(td);
        }
        
        if (y->l != NULL) {
            x = y->l;
        }
        else {
            x = y->r;
        }
        
        if (x != NULL)
            x->p = y->p;
        
        if (y->p == NULL) {
            t->root = x;
        }
        else if (y == y->p->l) {
            y->p->l = x;
        }
        else {
            y->p->r = x;
        }
        
        if (y != td) {
            td->key = y->key;
        }
        
        if (y->c == 1)
            rb_tree_del_fixup(t, x);
    }
}

void rb_tree_insert(red_black_tree* t, int key) {
    red_black_node* new_node = (red_black_node*) malloc(sizeof(red_black_node));
    new_node->key = key;
    new_node->c = 0;
    new_node->l = NULL;
    new_node->r = NULL;
    new_node->p = NULL;
    
    red_black_node* y = NULL;
    red_black_node* x = t->root;
    
    while (x != NULL) {
        y = x;
        if (new_node->key < x->key) {
            x = x->l;
        }
        else {
            x = x->r;
        }
    }
    
    new_node->p = y;
    
    if (y == NULL) {
        t->root = new_node;
    }
    else if (new_node->key < y->key) {
        y->l = new_node;
    }
    else {
        y->r = new_node;
    }
    
    rb_tree_ins_fixup(t, new_node);
}

void print_rb_tree_from_root(red_black_node* root) {
    if (root != NULL) {
        print_rb_tree_from_root(root->l);
        printf("%d ", root->key);
        print_rb_tree_from_root(root->r);
    }
}

red_black_node* successor(red_black_node* node) {
    red_black_node* y = NULL;
    
    if (node->r != NULL)
        return min(node->r);
    y = node->p;
    
    while (y != NULL && node == y->r) {
        node = y;
        y = y->p;
    }
    return y;
}

red_black_node* min(red_black_node* n) {
    red_black_node* node = n;
    
    while (node->l != NULL) {
        node = node->l;
    }
    return node;
}