#include "red_black_tree.h"
#define NLL printf("\n");
#include <time.h>
#include <unistd.h>

//int main(void) {
//    
//    red_black_tree t;
//    t.root = NULL;
//    
//    rb_tree_insert(&t, 5);
//    rb_tree_insert(&t, 10);
//    rb_tree_insert(&t, 1);
//    rb_tree_insert(&t, 0);
//    rb_tree_insert(&t, 12);
//    
//    print_rb_tree_from_root(t.root);
//    NLL;
//    
//    red_black_node* n = rb_tree_search(&t, 10);
//    print_rb_tree_from_root(n);
//    NLL;
//    
//    n = rb_tree_search(&t, 5);
//    print_rb_tree_from_root(n);
//    NLL;
//    
//    
//    rb_tree_delete(&t, t.root->key);
//    print_rb_tree_from_root(t.root);
//    printf("[%d]", t.root->key);
//    NLL;
//    
//    rb_tree_delete(&t, 0);
//    rb_tree_delete(&t, 12);
//    print_rb_tree_from_root(t.root);
//    NLL;
//    
//    
//    time_t tt;
//    srand((unsigned) time(&tt));
//    
//    for(int i = 0; i < 30; ++i) {
//        usleep(500);
//        int foo = rand() % 500;
//        usleep(500);
//
//        
//        if (foo % 7 == 0) {
//            rb_tree_insert(&t, foo);
//        }
//        else {
//            rb_tree_delete(&t, foo);
//        }
//        print_rb_tree_from_root(t.root);
//        NLL;
//    }
//    
    
//    return 0;
//}

#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <unistd.h>

#define SEARCHERS 2
#define INSERTERS 2
#define DELETERS 2


pthread_mutex_t mutex, mutex_searchers, mutex_inserters, mutex_order;
sem_t sem_searchers;
red_black_tree t;

void *search(void* key) {
    while (1) {
        int searchers;

        pthread_mutex_lock(&mutex_order);
        pthread_mutex_lock(&mutex_searchers);
        sem_getvalue(&sem_searchers, &searchers);
        if (searchers == 0)                    
            pthread_mutex_lock(&mutex);         
        sem_post(&sem_searchers);                 
        pthread_mutex_unlock(&mutex_order);
        pthread_mutex_unlock(&mutex_searchers);
        
        red_black_node* node = rb_tree_search(&t, *((int*)key));
        
        if (node != NULL)
            printf("Searcher found: %d\n", node->key);
        else
            printf("Searcher could not found such node: %d \n", *((int*)key));
        
        pthread_mutex_lock(&mutex_inserters);
        sem_wait(&sem_searchers);
        sem_getvalue(&sem_searchers, &searchers);
        if (searchers == 0)               
            pthread_mutex_unlock(&mutex);
        pthread_mutex_unlock(&mutex_searchers);
    }
}

void *insert(void* key) {
    while (1) {
        pthread_mutex_lock(&mutex_order);
        pthread_mutex_lock(&mutex);
        pthread_mutex_unlock(&mutex_order);
        
        rb_tree_insert(&t, *((int*) key));
        printf("Inserter just saved: %d\n", *((int*) key));
        
        pthread_mutex_unlock(&mutex);
    }
}


void *delete(void* key) {
    while (1) {
        pthread_mutex_lock(&mutex_order);
        pthread_mutex_lock(&mutex);
        pthread_mutex_unlock(&mutex_order);
        
        rb_tree_delete(&t, *((int*) key));
        printf("%d Should be deleted\n", *((int*) key));
        
        pthread_mutex_unlock(&mutex);
    }
}

int main() {
    int i;

    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    pthread_mutex_init(&mutex, NULL);
    pthread_mutex_init(&mutex_searchers, NULL);
    pthread_mutex_init(&mutex_order, NULL);
    sem_init(&sem_searchers, 0, 0);

    pthread_t threads[SEARCHERS + INSERTERS + DELETERS];
    
    
    for (i = 0; i < SEARCHERS; ++i)
        pthread_create(&threads[i], &attr, search, (void*) &i);
    for (int j = 0; j < INSERTERS; ++j, ++i)
        pthread_create(&threads[i], &attr, insert, (void*) &j);
    for (int j = 0; j < DELETERS; ++j, ++i)
        pthread_create(&threads[i], &attr, delete, (void*) &j);



    for (i = 0; i < SEARCHERS + INSERTERS + DELETERS; ++i)
        pthread_join(threads[i], NULL);

    pthread_attr_destroy(&attr);
    pthread_mutex_destroy(&mutex);
    pthread_mutex_destroy(&mutex_searchers);
    pthread_mutex_destroy(&mutex_order);
    sem_destroy(&sem_searchers);

    pthread_exit(NULL);
    return 0;
}
