#ifndef RED_BLACK_TREE_H
#define RED_BLACK_TREE_H

#include <stdlib.h>
#include <stdio.h>

typedef struct red_black_node red_black_node;

struct red_black_node {
    int key;
    int c; // 1 - black, 0 -red
    red_black_node* l;
    red_black_node* r;
    red_black_node* p;
    
};

typedef struct red_black_tree_struct {
    red_black_node* root;
} red_black_tree;

void rotate_left(red_black_tree* t, red_black_node* node);
void rotate_right(red_black_tree* t, red_black_node* node);

void rb_tree_ins_fixup(red_black_tree* t, red_black_node* node);
void rb_tree_del_fixup(red_black_tree* t, red_black_node* node);

red_black_node* rb_tree_search(red_black_tree* t, int key);
void rb_tree_delete(red_black_tree* t, int key);
void rb_tree_insert(red_black_tree* t, int key);

void print_rb_tree_from_root(red_black_node* root);
red_black_node* successor(red_black_node* node);
red_black_node* min(red_black_node* n);

#endif /* RED_BLACK_TREE_H */

